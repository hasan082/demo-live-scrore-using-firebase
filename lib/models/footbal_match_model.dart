class FootBallMatch {
  final String team1;
  final String team2;
  final String time;
  final String totalTime;
  final List<Goal> goals;

  FootBallMatch({
    required this.team1,
    required this.team2,
    required this.time,
    required this.totalTime,
    required this.goals,
  });

  factory FootBallMatch.fromFirestore(Map<String, dynamic> data) {
    List<dynamic> goalsData = data['goals'] ?? [];
    List<Goal> goals = goalsData.map((goalData) {
      return Goal(
        count: goalData['count'] ?? 0,
      );
    }).toList();

    return FootBallMatch(
      team1: data['team1'] ?? '',
      team2: data['team2'] ?? '',
      time: data['time'] ?? '0.0',
      totalTime: data['total_time'] ?? '0.0',
      goals: goals,
    );
  }
}

class Goal {
  final int count;
  Goal({
    required this.count,
  });
}
