import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:live_scrore_using_firebase/hidden_dir_hidden/add_list_screen.dart';
import '../controller/score_sontroller.dart';
import '../models/footbal_match_model.dart';
import 'match_detail_screen.dart';



class MatchList extends StatelessWidget {


  MatchList({Key? key}) : super(key: key);

  final MatchListController controller = Get.put(MatchListController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Match List'),
        actions: [
          IconButton(
              onPressed: (){
                Get.to(()=>AddMatchListScreen());
              },
              icon: const Icon(Icons.next_plan_outlined))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: StreamBuilder<List<FootBallMatch>>(
          stream: controller.fetchMatchesStream(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              log('${snapshot.data}');
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (!snapshot.hasData) {
              return const Center(
                child: Text('No matches available.'),
              );
            }
            if (snapshot.hasError) {
              return Center(
                child: Text('An error occurred: ${snapshot.error}'),
              );
            }
            return ListView.separated(
              shrinkWrap: true,
              physics: const BouncingScrollPhysics(),
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index) {
                var match = snapshot.data![index];
                return ListTile(
                  contentPadding: EdgeInsets.zero,
                  title: Text(
                    '${match.team1} vs ${match.team2}',
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  trailing: const Icon(Icons.arrow_forward),
                  onTap: () {
                    Get.to(() => MatchDetailsScreen(match: match));
                  },
                );
              }, separatorBuilder: (BuildContext context, int index) {
                return const Divider(height: 0,thickness: 1,);
            },
            );
          },

        ),
      ),
    );
  }
}
