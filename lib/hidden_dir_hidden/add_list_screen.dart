import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';


class AddMatchListScreen extends StatelessWidget {
  AddMatchListScreen({super.key});

  final List<Map<String, dynamic>> matchLists = [
    {
      'team1': 'Argentina',
      'team2': 'Africa',
      'time': '32.47',
      'total_time': '90.00',
      'goals': [
        {'count': 4},
        {'count': 2},
      ],
    },
    {
      'team1': 'Brazil',
      'team2': 'Germany',
      'time': '45.00',
      'total_time': '90.00',
      'goals': [
        {'count': 3},
        {'count': 2},
      ],
    },
    {
      'team1': 'Spain',
      'team2': 'Italy',
      'time': '55.00',
      'total_time': '90.00',
      'goals': [
        {'count': 2},
        {'count': 1},
      ],
    },
    {
      'team1': 'France',
      'team2': 'England',
      'time': '60.00',
      'total_time': '90.00',
      'goals': [
        {'count': 2},
        {'count': 2},
      ],
    },
    {
      'team1': 'Portugal',
      'team2': 'Netherlands',
      'time': '72.00',
      'total_time': '90.00',
      'goals': [
        {'count': 1},
        {'count': 3},
      ],
    },
    {
      'team1': 'Belgium',
      'team2': 'Spain',
      'time': '85.00',
      'total_time': '90.00',
      'goals': [
        {'count': 2},
        {'count': 2},
      ],
    },
    {
      'team1': 'Italy',
      'team2': 'Germany',
      'time': '28.00',
      'total_time': '90.00',
      'goals': [
        {'count': 1},
        {'count': 2},
      ],
    },
    {
      'team1': 'Brazil',
      'team2': 'France',
      'time': '53.00',
      'total_time': '90.00',
      'goals': [
        {'count': 3},
        {'count': 1},
      ],
    },
    {
      'team1': 'Argentina',
      'team2': 'Spain',
      'time': '67.00',
      'total_time': '90.00',
      'goals': [
        {'count': 2},
        {'count': 2},
      ],
    },
  ];



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Match List'),
        leading: IconButton(
          onPressed: () {
            Get.back(result: true);
        }, icon: const Icon(Icons.arrow_back),
        ),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () async {
            // Call the function to insert match lists
            await insertMatchLists(matchLists);
          },
          child: const Text('Insert Match Lists'),
        ),
      ),
    );
  }



  Future<void> insertMatchLists(List<Map<String, dynamic>> matchLists) async {
    try {
      final firestore = FirebaseFirestore.instance;

      for (int i = 0; i < matchLists.length; i++) {
        final customId = 'match${i + 11}';
        await firestore.collection('matches').doc(customId).set(matchLists[i]);
      }

      log('Match lists inserted successfully.');
    } catch (error) {
      log('Error inserting match lists: $error');
    }
  }





}
