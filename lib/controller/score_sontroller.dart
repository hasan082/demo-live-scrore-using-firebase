import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import '../models/footbal_match_model.dart';

class MatchListController extends GetxController {

  final matches = <FootBallMatch>[].obs;

  Stream<List<FootBallMatch>> fetchMatchesStream() {
    return FirebaseFirestore.instance.collection('matches').snapshots().map(
          (querySnapshot) {
        return querySnapshot.docs.map((doc) {

          return FootBallMatch.fromFirestore(doc.data());
        }).toList();
      },
    );
  }
}



