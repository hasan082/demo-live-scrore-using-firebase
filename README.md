# Live Score App using Firebase and GetX

This is a Flutter app that displays live football match scores using Firebase Firestore for data storage and GetX for state management.

## Features

- Display a list of live football matches with team names.
- Tap on a match to view more details.
- Real-time updates using Firebase Firestore and StreamBuilder.
- State management using GetX for reactive programming.

## Screenshots
<img src="https://gitlab.com/hasan082/demo-live-scrore-using-firebase/-/raw/main/screenshot_1.png" width="160" />

<img src="https://gitlab.com/hasan082/demo-live-scrore-using-firebase/-/raw/main/screenshot_2.png" width="160" />

## Getting Started

1. Clone this repository to your local machine using `git clone`.
2. Navigate to the project directory using the terminal.
3. Run `flutter pub get` to install the project dependencies.
4. Open an Android emulator or connect a physical device.
5. Run `flutter run` to launch the app.

## Dependencies

- `cloud_firestore`: Firebase Firestore SDK for data storage.
- `get`: GetX library for state management and reactive programming.
- `fluttertoast`: To show toast messages.
- Add any other dependencies you might need for your specific project.

## Project Structure

The project follows the standard Flutter project structure:

- `lib/`: Contains the Dart code for the app.
  - `controllers/`: Contains the GetX controllers.
  - `models/`: Contains the data models.
  - `screens/`: Contains the different screens of the app.
  - `main.dart`: The entry point of the app.

## How It Works

1. The app fetches live football match data from Firebase Firestore.
2. The `MatchListController` manages the state of the matches using GetX.
3. The `StreamBuilder` listens to changes in the Firestore stream and updates the UI reactively.
4. Users can tap on a match to view more details.

## Contributions

Contributions are welcome! If you find any issues or have suggestions for improvements, please open an issue or submit a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
